Firefox/Chrome add-on for resizing images inline by holding the right mouse button and dragging. Right mouse button + left mouse button = fit to screen. Right mouse + middle mouse button = reset size.

Uses the new WebExtensions API.

The add-on can be installed at:
https://addons.mozilla.org/en-US/firefox/addon/image-zoomer/