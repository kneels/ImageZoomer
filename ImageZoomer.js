var resizing, didResize;
var startPos, startPosSet;
var target;
var imgWidth, imgHeight;
var mouseX, mouseY;
var origOnclick, origOnclickAttr;

window.addEventListener("mousedown", function (evt) {
    if (evt.button === 2 && evt.target.tagName === 'IMG') { /* Start resize */
        resizing = true;
        target = evt.target;
        [imgWidth, imgHeight] = [0, 0];
        [mouseX, mouseY] = [0, 0];
        window.addEventListener("mouseup", MouseUpHandler);
        window.addEventListener("click", ClickHandler);
        window.addEventListener("contextmenu", ContextMenuHandler);
        window.addEventListener("mousemove", MouseMoveHandler);
    } else if (resizing && evt.button === 1) { /* Reset size */
        setImageSize(target, getOriginalImageSize(target));
        RemoveOnclick();
        resizing = false;
        didResize = true;
        startPosSet = false;
    } else if (resizing && evt.button === 0) { /* Fit to screen */
        evt.preventDefault();
        RemoveOnclick();
        fitToScreen();
    }
});

function MouseUpHandler(evt) {
    if (resizing && evt.button === 2) {
        resizing = false;
        startPosSet = false;
        window.removeEventListener("mouseup", MouseUpHandler);
    } else if (didResize && (evt.button === 0 || evt.button === 1)) {
        setTimeout(RestoreOnclick, 50);
    }
}

function ClickHandler(evt) {
    if (didResize) {
        evt.preventDefault();
        window.removeEventListener("click", ClickHandler);
    }
}

function ContextMenuHandler(evt) {
    if (didResize) {
        evt.preventDefault();
        didResize = false;
        window.removeEventListener("contextmenu", ContextMenuHandler);
    }
}

function RemoveOnclick() {
    origOnclickAttr = target.getAttribute("onclick");
    origOnclick = target.onclick;
    target.onclick = (function (e) {
        e.preventDefault();
        e.stopPropagation();
    })
}

function RestoreOnclick() {
    target.onclick = origOnclick;
    target.setAttribute("onclick", origOnclickAttr);
}

function DragResize() {
    if (resizing && null !== target) {
        const minWidthHeight = 50;
        var dist = (mouseX + mouseY) - startPos;
        var ratio = imgWidth / imgHeight;
        var w = Math.max(imgWidth + dist, minWidthHeight);
        var h = Math.ceil(w / ratio);

        setImageSize(target, [w, h]);
        requestAnimationFrame(DragResize);
        didResize = true;
    }
}

function MouseMoveHandler(evt) {
    if (!resizing) {
        window.removeEventListener("mousemove", MouseMoveHandler);
        return;
    }

    if (!startPosSet) {
        startPos = evt.pageX + evt.pageY;
        startPosSet = true;
        [imgWidth, imgHeight] = getCurrentImageSize(target);

        if (!hasBeenResized(target)) {
            saveDimensions(target, [imgWidth, imgHeight]);
        }
        requestAnimationFrame(DragResize);
    }

    [mouseX, mouseY] = [evt.pageX, evt.pageY];
}

function fitToScreen() {
    [imgWidth, imgHeight] = getCurrentImageSize(target);

    if (!hasBeenResized(target)) {
        saveDimensions(target, [imgWidth, imgHeight]);
    }

    var ratio = imgWidth / imgHeight;
    var [ww, wh] = [window.innerWidth, window.innerHeight];
    var wratio = ww / wh;

    if (wratio > ratio) {
        imgHeight = wh;
        imgWidth = imgHeight * ratio;
    } else {
        imgWidth = ww;
        imgHeight = (imgWidth / ratio);
    }

    setImageSize(target, [imgWidth, imgHeight]);
    target.scrollIntoView(true);

    didResize = true;
}

function getCurrentImageSize(img) {
    var w = img.width;
    var h = img.height;
    if (!img.style.width.includes('%') && !img.style.height.includes('%')) {
        w = parseInt(img.style.width) || w;
        h = parseInt(img.style.height) || h;
    }
    return [w, h];
}

function getOriginalImageSize(img) {
    var w = parseInt(img.getAttribute('origWidth'));
    var h = parseInt(img.getAttribute('origHeight'));

    if (isNaN(w) || isNaN(h)) {
        return getCurrentImageSize(img);
    }
    return [w, h];
}

function setImageSize(img, wh) {
    var styleStr = 'width:' + wh[0] + 'px !important;height:' + wh[1] + 'px !important;max-width:none \
        !important;max-height:none !important;min-width:0 !important;min-height:0 !important';

    if (wh[1] > window.innerHeight) {
        styleStr += ';margin-top:0 !important';
    }
    img.setAttribute('style', styleStr);
}

function saveDimensions(img, wh) {
    img.setAttribute('origWidth', wh[0]);
    img.setAttribute('origHeight', wh[1]);
}

function hasBeenResized(img) {
    return null != img.getAttribute('origWidth');
}